# Doki

A wiki.

## Usage

You have to have Vagrant and Fabric installed.

To bring up the Vagrant box, provision it and start the site run:

    fab

To bring it down:

    vagrant halt

## License

MIT
